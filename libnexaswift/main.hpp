//
//  main.hpp
//  libnexaswift
//
//  Created by Jørgen Svennevik Notland on 21/03/2024.
//

#ifndef main_hpp
#define main_hpp

#include <stdio.h>

#endif /* main_hpp */

#ifdef __cplusplus
extern "C" {
#endif

const char* testMain();

#ifdef __cplusplus
} // extern "C"
#endif
