//
//  libnexaswiftApp.swift
//  libnexaswift
//
//  Created by Jørgen Svennevik Notland on 21/03/2024.
//

import SwiftUI

@main
struct libnexaswiftApp: App {
    init() {
        testMain()
    }
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
