//
//  ContentView.swift
//  libnexaswift
//
//  Created by Jørgen Svennevik Notland on 21/03/2024.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Image(systemName: "globe")
                .imageScale(.large)
                .foregroundStyle(.tint)
            Text("\(String(cString: testMain()))")
        }
        .padding()
    }
}

#Preview {
    ContentView()
}
